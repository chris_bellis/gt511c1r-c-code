#include "gt511c1r_natcode_NativeFingerprint.h"
#include "CommBase.h"
#include "OEM.h"

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    open
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_gt511c1r_natcode_NativeFingerprint_open
  (JNIEnv * jne, jobject obj)
{
    bool res = comm_open_usb();
    if(res)
	{
        int res2 = oem_open();
        if(res2 == OEM_COMM_ERR)
		{
            res = false;
		}
	}
    return res;
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    close
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_close
  (JNIEnv * jne, jobject obj)
{
    return oem_close();
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    internal_check
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_internal_1check
  (JNIEnv * jne, jobject obj)
{
    return oem_usb_internal_check();
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    change_baudrate
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_change_1baudrate
  (JNIEnv * jne, jobject obj, jint rate)
{
    return oem_change_baudrate(rate);
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    cmos_led
 * Signature: (Z)I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_cmos_1led
  (JNIEnv * jne, jobject obj, jboolean led)
{
    return oem_cmos_led(led);
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    enroll_count
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_enroll_1count
  (JNIEnv * jne, jobject obj)
{
	return oem_enroll_count();
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    check_enrolled
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_check_1enrolled
  (JNIEnv * jne, jobject obj, jint enroll)
{
	return oem_check_enrolled(enroll);
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    enroll_start
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_enroll_1start
  (JNIEnv * jne, jobject obj, jint enroll)
{
	return oem_enroll_start(enroll);
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    enroll_nth
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_enroll_1nth
  (JNIEnv * jne, jobject obj, jint enroll, jint sample_num)
{
	return oem_enroll_nth(enroll, sample_num);
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    is_press_finger
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_is_1press_1finger
  (JNIEnv * jne, jobject obj)
{
	return oem_is_press_finger();
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    delete_id
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_delete_1id
  (JNIEnv * jne, jobject obj, jint id)
{
	return oem_delete(id);
}


/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    delete_all
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_delete_1all
  (JNIEnv * jne, jobject obj)
{
    return oem_delete_all();
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    verify
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_verify
  (JNIEnv * jne, jobject obj, jint id)
{
	return oem_verify(id);
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    identify
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_identify
  (JNIEnv * jne, jobject obj)
{
	return oem_identify();
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    verify_template
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_verify_1template
  (JNIEnv * jne, jobject obj, jint id)
{
	return oem_verify_template(id);
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    identify_template
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_identify_1template
  (JNIEnv * jne, jobject obj, jint id)
{
	return oem_identify_template();
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    capture
 * Signature: (Z)I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_capture
  (JNIEnv * jne, jobject obj, jboolean best)
{
	return oem_capture(best);
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    get_image
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_get_1image
  (JNIEnv * jne, jobject obj)
{
	return oem_get_image();
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    get_image_data
 * Signature: ()[B
 */
JNIEXPORT jbyteArray JNICALL Java_gt511c1r_natcode_NativeFingerprint_get_1image_1data
  (JNIEnv * jne, jobject obj)
{
	jbyte imgdata[IMG8BIT_SIZE];
    for(int i = 0; i<IMG8BIT_SIZE; i++)
	{
		imgdata[i] = (jbyte)gbyImg8bit[i];
	}
	
	jbyteArray outArray = jne->NewByteArray(IMG8BIT_SIZE);
    if(NULL == outArray) 
	{
		return NULL;
	}
	jne->SetByteArrayRegion(outArray, 0, IMG8BIT_SIZE, (jbyte*)imgdata);
	return outArray;
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    get_rawimage
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_get_1rawimage
  (JNIEnv * jne, jobject obj)
{
	return oem_get_rawimage();
}
/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    get_rawimage_data
 * Signature: ()[B
 */
JNIEXPORT jbyteArray JNICALL Java_gt511c1r_natcode_NativeFingerprint_get_1rawimage_1data
  (JNIEnv * jne, jobject obj)
{
	jbyte imgdata[IMG8BIT_SIZE];
    for(int i = 0; i<IMG8BIT_SIZE; i++)
	{
		imgdata[i] = (jbyte)gbyImgRaw[i];
	}
	
	jbyteArray outArray = jne->NewByteArray(IMG8BIT_SIZE);
    if(NULL == outArray) 
	{
		return NULL;
	}
	jne->SetByteArrayRegion(outArray, 0, IMG8BIT_SIZE, (jbyte*)imgdata);
	return outArray;
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    get_template
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_get_1template
  (JNIEnv * jne, jobject obj, jint id)
{
	return oem_get_template(id);
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    add_template
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_add_1template
  (JNIEnv * jne, jobject obj, jint id)
{
	return oem_add_template(id);
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    set_template_data
 * Signature: ([B)V
 */
JNIEXPORT void JNICALL Java_gt511c1r_natcode_NativeFingerprint_set_1template_1data
  (JNIEnv * jne, jobject obj, jbyteArray)
{
    // Nothign to do here
    // TODO: Implement adding templates
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    get_database_start
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_get_1database_1start
  (JNIEnv * jne, jobject obj)
{
	return oem_get_database_start();
}


/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    get_database_end
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_get_1database_1end
  (JNIEnv * jne, jobject obj)
{
	return oem_get_database_end();
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    get_last_ack
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_get_1last_1ack
  (JNIEnv * jne, jobject obj)
{
	return gwLastAck;
}

/*
 * Class:     gt511c1r_natcode_NativeFingerprint
 * Method:    get_last_ack_param
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_gt511c1r_natcode_NativeFingerprint_get_1last_1ack_1param
  (JNIEnv * jne, jobject obj)
{
	return gwLastAckParam;
}